import React from 'react'
import { createStyles } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { RoomTabs, RoomMenuActions } from '../../constants/rooms'
import RoomModal from './RoomModal'
import { actions } from '../../utils/actionCreator'
import classnames from 'classnames'
import RoomCard from './RoomCard'
import BookingLog from './BookingLog'

import {
  AppBar,
  Tabs,
  Tab,
  Button,
  Grid
} from '@material-ui/core'

class Rooms extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      activeTab: 0,
      tabs: RoomTabs[props.user.language],
      showRoomModal: false
    }
  }

  componentDidMount() {
    const { fetchRooms, fetchAllEventsForToday, fetchAllEvents } = this.props
    fetchRooms()
    fetchAllEventsForToday()
    fetchAllEvents()
  }

  changeActiveTab = (event, tabNumber) => {
    event.preventDefault()
    this.setState(state => state.activeTab === tabNumber ? null : ({activeTab: tabNumber}))
  }

  toggleRoomModal = () => this.setState(state => ({ showRoomModal: !state.showRoomModal }))

  render () {
    const { classes, rooms, eventsInAllRoomForToday, allEvents, user: { language } } = this.props
    const { activeTab, tabs, showRoomModal } = this.state

    return (
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={activeTab}
            onChange={this.changeActiveTab}>
            {tabs.map(tabLabel => <Tab key={tabLabel} label={tabLabel} />)}
          </Tabs>
        </AppBar>
        {activeTab === 0 && <div className={classes.typography}>
          <Button className={classes.layout} variant="contained" size="small" color="primary" onClick={this.toggleRoomModal}>
            {RoomMenuActions[language].ADD_NEW_ROOM}
          </Button>
          <div className={classnames(classes.layout, classes.cardGrid)}>
            <Grid container spacing={24}>
              {rooms.map(room => (
                <RoomCard
                  events={eventsInAllRoomForToday.filter(event => event.roomId === room._id)}
                  key={room._id}
                  room={room}
                />
              ))}
            </Grid>
          </div>
        </div>}
        {activeTab === 1 && <div className={classes.typography}>
          <Grid container spacing={24}>
            <BookingLog events={allEvents} />
          </Grid>
        </div>}
        {showRoomModal && <RoomModal
          room={null}
          open={showRoomModal}
          toggleRoomModal={this.toggleRoomModal}
        />}
      </div>
    )
  }
}

const styles = theme => createStyles({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  typography: {
    padding: theme.spacing.unit * 3,
    border: "1px solid #eee"
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  cardGrid: {
    padding: `${theme.spacing.unit * 2}px 0`
  }
})

const mapStateToProps = state => {
  return {
    user: state.userReducer.user,
    rooms: state.roomReducer.rooms,
    allEvents: state.eventReducer.allEvents,
    eventsInAllRoomForToday: state.eventReducer.eventsInAllRoomForToday
  }
}

const mapDispatchToProps = dispatch => ({
  fetchRooms: () => dispatch(actions.FETCH_ROOMS.REQUEST()),
  fetchAllEventsForToday: () => dispatch(actions.FETCH_ALL_EVENTS_FOR_TODAY.REQUEST()),
  fetchAllEvents: (data) => dispatch(actions.FETCH_ALL_EVENTS.REQUEST(data))
})

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles, { withTheme: true })(Rooms)))