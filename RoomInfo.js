import React from 'react'
import { createStyles } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

import {
  Slide,
  Dialog,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Button
} from '@material-ui/core'

import CloseIcon from '@material-ui/icons/Close'

const Transition = props => {
  return <Slide direction="down" {...props} />
}

class RoomInfo extends React.Component {
  constructor(props){
    super(props)
    this.state = {}
  }

  render() {
    const {
      classes,
      open,
      toggleRoomInfoModal,
      room
    } = this.props

    return (
      <React.Fragment>
        <Dialog
          fullScreen
          open={open}
          onClose={toggleRoomInfoModal}
          TransitionComponent={Transition}
        >
          <AppBar className={classes.appBar}>
            <Toolbar>
              <IconButton color="inherit" onClick={toggleRoomInfoModal} aria-label="Close">
                <CloseIcon />
              </IconButton>
              <Typography variant="h6" color="inherit" className={classes.flex}>
                {room.name}
              </Typography>
              <Button color="inherit" onClick={toggleRoomInfoModal}>
                Close
              </Button>
            </Toolbar>
          </AppBar>
        </Dialog>
      </React.Fragment>
    )
  }
}

const styles = theme => createStyles({
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
    marginLeft: theme.spacing.unit
  }
})

export default withRouter(connect(
  null,
  null
)(withStyles(styles, { withTheme: true })(RoomInfo)))