import React from 'react'
import { createStyles } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { withRouter } from 'react-router-dom'
import { RoomMenuActions, Rooms, ConfirmationMessages } from '../../constants/rooms'
import RoomModal from './RoomModal'
import { actions } from '../../utils/actionCreator'
import { connect } from 'react-redux'
import EventDialog from '../events/EventDialog'
import * as _ from 'lodash'
import moment from 'moment'
import RoomInfo from './RoomInfo'
import AlertDialog from '../../components/global/AlertDialog'

import {
  Grid,
  Card,
  CardHeader,
  Avatar,
  IconButton,
  Menu,
  MenuItem,
  CardMedia,
  CardContent,
  Typography,
  CardActions,
  Button
} from '@material-ui/core'

import MoreVertIcon from '@material-ui/icons/MoreVert'
import red from '@material-ui/core/colors/red'

class RoomCard extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      anchorEl: null,
      showRoomModal: false,
      editingRoom: null,
      showAddEventDialog: false,
      showRoomInfo: false,
      startDate: new Date(),
      events: this.orderEventsByTime(props.events),
      showAlertDialogModal: false
    }
  }

  componentWillReceiveProps(newProps) {
    this.setState({ events: this.orderEventsByTime(newProps.events) })
  }

  handleAlertDialog = () => this.setState(state => ({ showAlertDialogModal: !state.showAlertDialogModal }))

  orderEventsByTime = (eventList, sort) => {
    return _.orderBy(eventList, o => {
      return moment(o.eventTime[0], 'h:mm A').format('HH:mm')
    }, [sort])
  }

  handleMenu = event => this.setState({ anchorEl: event.currentTarget })

  toggleRoomModal = () => this.setState(state => ({ showRoomModal: !state.showRoomModal }))

  getNearestEvent = () => {
    const { events } = this.state
    const { room: { _id }, classes, user: { language } } =this.props

    if (events.length === 0) {
      return (
        <Typography variant="body2" gutterBottom>
          {Rooms[language].EVENTS_NOT_FOUND}
        </Typography>
      )
    } else {
      const event = events.find(e => e.roomId === _id)
      if (event) {
        return (
          <div>
            <Typography className={classes.truncate} variant="body2" gutterBottom>
              {`${event.name}`}
            </Typography>
            <Typography variant="body2" gutterBottom>
              {Rooms[language].TIME} {`${event.eventTime[0]} - ${event.eventTime[event.eventTime.length - 1]}`}
            </Typography>
            <Typography variant="body2" gutterBottom>
            {Rooms[language].CREATED_BY}: {event.user.firstName} {event.user.lastName}
            </Typography>
          </div>
        )
      } else {
        return (
          <Typography variant="body1" gutterBottom>
            {Rooms[language].EVENTS_NOT_FOUND}
          </Typography>
        )
      }
    }
  }

  handleClose = () => this.setState({ anchorEl: null })

  toggleRoomInfoModal = () => this.setState(state => ({ showRoomInfo: !state.showRoomInfo }))

  toggleEventDialog = () => this.setState(state => ({ showAddEventDialog: !state.showAddEventDialog }))

  editRoom = (event, editingRoom) => {
    event.preventDefault()
    this.handleClose()
    this.setState({ editingRoom }, () => this.toggleRoomModal(event))
  }

  removeRoomById = (event, roomId) => {
    event.preventDefault()
    const { removeRoomById } = this.props
    this.handleClose()
    removeRoomById({ roomId })
  }

  render () {
    const { classes, room, user: { language } } = this.props
    const { anchorEl, showRoomModal, editingRoom, showAddEventDialog, showRoomInfo, showAlertDialogModal } = this.state
    const open = Boolean(anchorEl)

    const { title, msg } = ConfirmationMessages[language].REMOVE_ROOM

    return (
      <Grid item sm={6} md={4} lg={3}>
        <Card className={classes.card}>
          <CardHeader
            avatar={
              <Avatar aria-label="Room" className={classes.avatar}>
                R
              </Avatar>
            }
            action={
              <React.Fragment>
                <IconButton onClick={this.handleMenu}>
                  <MoreVertIcon />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={open}
                  onClose={this.handleClose}
                >
                <MenuItem onClick={e => this.editRoom(e, room)}>{RoomMenuActions[language].EDIT}</MenuItem>
                <MenuItem onClick={this.handleAlertDialog}>{RoomMenuActions[language].DELETE}</MenuItem>
                {showAlertDialogModal ? <AlertDialog
                  open={showAlertDialogModal}
                  handleAlertDialog={this.handleAlertDialog}
                  title={title}
                  message={msg}
                  cancel={() => {
                    this.handleAlertDialog()
                    this.handleClose()
                  }}
                  confirm={e => this.removeRoomById(e, room._id)}
                /> : null}
              </Menu>
            </React.Fragment>
            }
            title={room.name}
          />
          <CardMedia
            className={classes.cardMedia}
            image={room.image}
            title="Image title"
          />
          <CardContent className={classes.cardContent}>
            <Typography variant="button" gutterBottom>
              {Rooms[language].NEAREST_EVENT}
            </Typography>
            {this.getNearestEvent()}
          </CardContent>
          <CardActions className={classes.actions} disableActionSpacing>
            {/* <Button variant="outlined" size="small" color="primary" className={classes.button} onClick={this.toggleRoomInfoModal}>
              {RoomMenuActions[language].OPEN_ROOM}
            </Button> */}
            <Button variant="outlined" size="small" color="secondary" className={classes.button} onClick={this.toggleEventDialog}>
              {RoomMenuActions[language].ADD_EVENT}
            </Button>
          </CardActions>
        </Card>
        {showRoomInfo && <RoomInfo
          room={room}
          open={showRoomInfo}
          toggleRoomInfoModal={this.toggleRoomInfoModal}
        />}
        {showAddEventDialog && <EventDialog
          room={room}
          open={showAddEventDialog}
          toggleEventDialog={this.toggleEventDialog}
        />}
        {showRoomModal && <RoomModal
          open={showRoomModal}
          toggleRoomModal={this.toggleRoomModal}
          room={editingRoom}
        />}
      </Grid>
    )
  }
}

const styles = theme => createStyles({
  card: {
    height: 'fit-content',
    display: 'flex',
    flexDirection: 'column'
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
    height: '140px'
  },
  cardAction: {
    justifyContent: 'flex-end'
  },
  fab: {
    margin: theme.spacing.unit,
  },
  avatar: {
    backgroundColor: red[500],
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  button: {
    margin: theme.spacing.unit,
  },
  truncate: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  }
})

const mapStateToProps = state => {
  return {
    user: state.userReducer.user
  }
}

const mapDispatchToProps = dispatch => ({
  removeRoomById: data => dispatch(actions.REMOVE_ROOM.REQUEST(data))
})

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles, { withTheme: true })(RoomCard)))