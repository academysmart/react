import * as React from 'react'
import { createStyles } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import moment from 'moment'
import * as _ from 'lodash'
import EventListInBookingLog from '../events/EventListInBookingLog'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

import {
  Stepper,
  Step,
  StepLabel,
  StepContent
} from '@material-ui/core'

class BookingLog extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      activePoint: 0
    }
  }

  orderEventsByTime = (eventList, sort) => {
    return _.orderBy(eventList, o => {
      return moment(o.eventTime[0], 'h:mm A').format('HH:mm')
    }, [sort])
  }

  orderByTime = (times, sort) => {
    return _.orderBy(times, o => {
      return moment(o, 'h:mm A').format('HH:mm')
    }, [sort])
  }

  render () {
    const { classes, events } = this.props
    const { activePoint } = this.state

    const dateList = this.orderEventsByTime(events, 'asc').map(event => moment(new Date(event.createdAt)).format('MM/DD/YYYY'))
    const uniqueDateList = dateList.filter((v, i, a) => a.indexOf(v) === i)

    return (
      <div className={classes.root}>
        <Stepper activeStep={activePoint} orientation="vertical">
          {this.orderByTime(uniqueDateList, 'asc').reverse().map((date, index) => (
            <Step key={date} active={index !== 3}>
              <StepLabel>{moment(new Date(date)).format('MM/DD/YYYY')}</StepLabel>
              <StepContent>
                <EventListInBookingLog eventList={events.filter(event => moment(new Date(event.createdAt)).format('MM/DD/YYYY') === moment(new Date(date)).format('MM/DD/YYYY'))} />
              </StepContent>
            </Step>
          ))}
        </Stepper>
      </div>
    )
  }
}

const styles = theme => createStyles({
  root: {
    width: '100%',
  }
})

const mapStateToProps = state => {
  return {
    user: state.userReducer.user
  }
}

export default withRouter(connect(
  mapStateToProps,
  null
)(withStyles(styles, { withTheme: true })(BookingLog)))