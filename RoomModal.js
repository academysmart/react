import React from 'react'
import { createStyles } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { withRouter } from 'react-router-dom'
import Dropzone from 'react-dropzone'
import { actions } from '../../utils/actionCreator'
import CloudDownloadIcon from '@material-ui/icons/CloudDownload'
import { connect } from 'react-redux'
import { RoomMenuActions, Rooms, UploadImage, Form, FormValidation } from '../../constants/rooms'
import { Buttons } from '../../constants/global'

import './styles/RoomModal.scss'

import {
  Dialog,
  Slide,
  DialogTitle,
  Typography,
  DialogContent,
  TextField,
  DialogActions,
  Button
} from '@material-ui/core'

const Transition = props => {
  return <Slide direction="down" {...props} />
}

class RoomModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      image: 'https://admin.cmf.org.uk/e8680ad1/08af6ed98876ae1c07fb187b8abe78aa6d3d04fa.jpg',
      errors: {}
    }
  }

  componentWillMount() {
    const { room } = this.props
    if(room) {
      const { name, image } = room
      this.setState({ name, image })
    }
  }

  handLeChangeTextField = event => {
    const { name, value } = event.target
    this.validateFormField(event.target)

    this.setState({ [name]: value })
  }

  validateFormField = ({ name, value }) => {
    const { errors } = this.state
    const { user: { language } } = this.props

    if (!value || value.length === 0) {
      this.setState(state => ({ errors: Object.assign({}, state.errors, { [name]: FormValidation[language].REQUIRED }) }))
      return false
    } else {
      const newErrorsObj = Object.assign({}, errors)
      delete newErrorsObj[name]
      this.setState({ errors: newErrorsObj })
      return true
    }
  }

  validateForm = () => {
    const { name } = this.state
    return this.validateFormField({ name: 'name', value: name })
  }

  onDropRoomImage = acceptedFiles => {
    const file = acceptedFiles[0]
    console.log('file =>', file)
    this.setState({ image: 'https://picsum.photos/200/300' })
    // TODO
  }

  createNewRoom = event => {
    const { createRoom, toggleRoomModal } = this.props
    const { name, image } = this.state

    if (!this.validateForm()) {
      return
    }

    createRoom({ name, image }, err => {
      if (err) {
        return
      }

      this.setState({ name: '', image: '', errors: {} })
      toggleRoomModal(event)
    })
  }

  updateRoomById = event => {
    event.preventDefault()
    const { updateRoomById, toggleRoomModal, room: { _id } } = this.props

    const editedRoom = {
      ...this.state,
      _id
    }

    if (!this.validateForm()) {
      return
    }

    updateRoomById({ editedRoom }, err => {
      if (err) {
        return
      }
      toggleRoomModal(event)
    })
  }

  render () {
    const { open, toggleRoomModal, room, classes, user: { language } } = this.props
    const { name, errors } = this.state

    return (
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={toggleRoomModal}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">
        <Typography variant="button" gutterBottom>
          {!room ? Rooms.ADD_MEETING_ROOM : Rooms.EDIT_MEETING_ROOM}
        </Typography>
        </DialogTitle>
        <DialogContent>
          <form className={classes.container} noValidate autoComplete="off">
            <TextField
              required
              error={!!errors.name}
              id="name"
              style={{ margin: 8 }}
              label={Form[language].ROOM_NAME}
              placeholder={Form[language].PLACEHOLDER_ROOM_NAME}
              name="name"
              fullWidth
              className={classes.textField}
              value={name}
              onChange={this.handLeChangeTextField}
              margin="normal"
              variant="outlined"
              InputLabelProps={{
                shrink: true,
              }}
            />
            {errors.name ? <div className={classes.errorField}>{errors.name}</div> : null}
            <Dropzone onDrop={this.onDropRoomImage}>
              {({getRootProps, getInputProps}) => (
                <div {...getRootProps()} className={classes.dragZone}>
                  <input {...getInputProps()} />
                  <Typography variant="body2" gutterBottom>
                    <CloudDownloadIcon color="primary" style={{ fontSize: 50 }} /><br />
                    {/* <CloudDownloadIcon className="jump" color="primary" style={{ fontSize: 50 }} /><br /> */}
                    {UploadImage[language].UPLOAD}
                  </Typography>
                </div>
              )}
            </Dropzone>
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => {
            
            if(room) {
              const { name, image } = room
              this.setState({ name, image })
            } else {
              this.setState({ name: '', image: '' })
            }
            toggleRoomModal()
            
          }} color="primary">
            {Buttons[language].CANCEL}
          </Button>
          <Button disabled={!!Object.keys(errors).length} onClick={room ? this.updateRoomById : this.createNewRoom} color="secondary">
            {room ? RoomMenuActions[language].EDIT_ROOM : RoomMenuActions[language].ADD_ROOM}
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}

const styles = theme => createStyles({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 400,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'column'
  },
  dragZone: {
    marginTop: theme.spacing.unit,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 400,
    height: 200,
    backgroundColor: '#eee',
    border: '1px solid #ccc',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    display: 'flex',
    cursor: 'pointer'
  },
  errorField: {
    marginLeft: '8px',
    marginRight: '8px',
    fontSize: '1rem',
    fontFamily: 'Roboto, Helvetica, Arial, sans-serif',
    color: '#f44336'
  }
})

const mapDispatchToProps = dispatch => ({
  createRoom: (data, callback) => dispatch(actions.CREATE_ROOM.REQUEST(data, callback)),
  updateRoomById: (data, callback) => dispatch(actions.UPDATE_ROOM.REQUEST(data, callback))
})

const mapStateToProps = state => {
  return {
    user: state.userReducer.user
  }
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles, { withTheme: true })(RoomModal)))